/* results.c -- Extract precision stat.
 *
 * Copyright (C) 2019  Pierre-Antoine Rouby
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <float.h>
#include <math.h>

#define cube(a) ((a)*(a))

double max      = -DBL_MAX;
double min      = DBL_MAX;
double mean     = .0;
double max_diff = .0;
double sd       = .0;
double vmedian  = .0;

int ival = 0;
double val[150];                /* FIXME: Use dynamic allocation. */


int
gnuplot_capture (char *filename, char *src)
{
  FILE *p = popen("gnuplot", "w");
  if (p == NULL)
    {
      fprintf(stderr, "[gnuplot] Failed to open pipe.\n");
      return -1;
    }

  fprintf(p,
          "set terminal png size 800,600 enhanced font 'Helvetica,12'\n"
          "set output '%s'\n", filename);
  /* fprintf (p, "set title '%s'\n", filename); */
  fprintf(p, "plot '%s', %.3lf with lines, %.3lf with line, %.3lf with lines,"
          " %.3lf with lines\n",
          src, mean, mean+sd, mean-sd, vmedian);

  pclose(p);

  return 0;
}

int
cmp_double (const void *x, const void *y)
{
  if (*(double*)x < *(double*)y)
    return -1;
  else if (*(double*)x > *(double*)y)
    return 1;

  return 0;
}

double
median (double *x, double n)
{
  int i = n/2;
  int j;

  /* Sort data */
  qsort(x, n, sizeof(double), cmp_double);

  return x[i];                  /* Return median data */
}

double
_n_x (double *x, double n)
{
  int i;
  double c;

  for (i = 0; i < n; i++)
    c += x[i];

  return (1/n) * c;
}

double
standard_dev (double *x, double n)
{
  double n_x = _n_x(x, n);
  double c = 0.0;
  int i;

  for (i = 0; i < n; i++)
    c += cube(x[i] - n_x);

  c = (1/n) * c;

  return sqrt(c);
}

double
read_precision (char *filename)
{
  FILE * file = fopen(filename, "r");
  if (file == NULL)
    {
      printf ("Failed to open %s: (%d) %s\n",
              filename, errno, strerror(errno));
      exit (EXIT_FAILURE);
    }

  double ret = 0.0;

  while (fscanf (file, "%lf", &ret) != EOF)
    {
      if (ret > max)
        max = ret;
      if (ret < min)
        min = ret;

      if (mean == .0)
        mean = ret;
      else
        {
          mean += ret;
          mean /= 2;
        }

      val[ival] = ret;
      ival += 1;
    }

  max_diff = max - min;

  sd = standard_dev (val, ival);

  vmedian = median(val, ival);

  fclose (file);
  return ret;
}

void
usage ()
{
  printf ("Usage: results <path/to/file.txt>\n");
  exit (EXIT_FAILURE);
}

int
main (int argc, char **argv)
{
  char *file;

  if (argc < 2)
    usage();
  else
    file = argv[1];

  read_precision(file);

  printf ("                  Max: %lf %%\n", max);
  printf ("                  Min: %lf %%\n", min);
  printf ("                 Mean: %lf %%\n", mean);
  printf ("               Median: %lf %%\n", vmedian);
  printf ("             Max diff: %lf %%\n", max_diff);
  printf ("   Standard Deviation: %lf %%\n", sd);

  gnuplot_capture("repartition.png", file);

  return 0;
}

/* results.c ends here. */
