;;; -*- scheme -*-
;;;
;;; TODO: Push on Guix repo

(use-modules ((guix licenses) #:prefix license:)
             (guix packages)
             (guix download)
             (guix build-system r)
             (gnu packages cran)
             (gnu packages geo)
             (gnu packages maths)
             (gnu packages pkg-config))

(define-public r-rgdal
  (package
    (name "r-rgdal")
    (version "1.4-4")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "rgdal" version))
       (sha256
        (base32
         "1my56hdc9x40ynxx1qwqwqxjvjxybmm00w4xg5gi8zgj19pffci5"))))
    (build-system r-build-system)
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (propagated-inputs
     `(("r-sp" ,r-sp)
       ("gdal" ,gdal)
       ("proj" ,proj.4)))
    (home-page "https://cran.r-project.org/web/packages/rgdal")
    (synopsis "")
    (description
     "")
    (license license:gpl2+)))

(package
  (name "r-rgnuplot")
  (version "1.0.3")
  (source
   (origin
     (method url-fetch)
     (uri (cran-uri "Rgnuplot" version))
     (sha256
      (base32
       "0mwpq6ibfv014fgdfsh3wf8yy82nzva6cgb3zifn3k9lnr3h2fj7"))))
  (build-system r-build-system)
  (native-inputs
   `(("pkg-config" ,pkg-config)))
  (propagated-inputs
   `(("r-colorspace" ,r-colorspace)
     ("r-png" ,r-png)
     ("r-rgdal" ,r-rgdal)
     ("r-sp" ,r-sp)
     ("gnuplot" ,gnuplot)))
  (home-page "https://cran.r-project.org/web/packages/rgnuplot")
  (synopsis "")
  (description
   "")
  (license license:gpl3+))
