#################
## * GNUPlot * ##
#################

gnuplot_3d_lines <- function(data, name = "output", type, mean = TRUE,
                             relative_delta = TRUE) {
    ## This function make 3d lines plot and proba law. DATA need to be
    ## a dataframe, with delta, new, pack and mean fields.

    v_delta <- data_to_number (data[,"delta"])
    v_new   <- data_to_number (data[,"new"])
    v_pack  <- data_to_number (data[,"pack"])
    v_mean  <- data_to_number (data[,"mean"])
    v_c     <- data_to_number (data[,"c"])
    v_cmax  <- data_to_number (data[,"cmax"])

    ## Pack number
    npack = 45

    ## Define mean pack functions
    if (mean) {
        fm <- make_log_pack_lambda(min(v_mean), max(v_mean), n = npack)
        ## Virtual mean pack function, return minimal mean value for
        ## pack number
        vfm <- get_value_log_pack(min(v_mean), max(v_mean), n = npack)
    }

    ## Define delta pack functions
    fd <- make_pack_lambda(min(v_delta), max(v_delta), n = npack)
    ## Virtual delta pack function, return minimal delta value for
    ## pack number
    vfd <- get_value_pack(min(v_delta), max(v_delta), n = npack)

    ## Count element for each log block
    vx <- mapply(function(x) 0, 1:(npack+1))
    for (i in 1:length(v_mean)) {
        x <- fm(v_mean[i])+1 # Index start to 1
        vx[x] <- vx[x] + 1   # Inc
    }

    dm <- mapply(function(x) 0, 1:(npack+1))
    dn <- mapply(function(x) 0, 1:(npack+1))
    ## Take relative delta
    if (relative_delta) {
        v_relative_delta <- mapply(function(x) 0, length(v_delta))
        for (i in 1:length(v_delta)) {
            if (v_c[i] == 0) {
                # Special case if c == 0 then take hard value as delta.
                v_relative_delta[i] <- v_delta[i] / v_cmax[i]
            }
            else {
                # Take relative delta
                v_relative_delta[i] <- v_delta[i] / v_c[i]
            }
        }

        ## Compute relative delta mean
        for (i in 1:length(v_delta)) {
            x <- fd(v_delta[i])+1
            ## Add all relative delta for pack x
            dm[x] <- (dm[x] + v_relative_delta[i])
            ## Count number of data
            dn[x] <- dn[x] + 1
        }
        for (i in 1:(npack+1)) {
            if (dn[i] == 0)
                dm[i] <- 1.0
            else
                dm[i] <- (dm[i] / dn[i]) # Div by data number
        }
    }
    ## Take hard delta value
    else {
        ## Delta mean by pack
        for (i in 1:length(v_delta)) {
            x <- fd(v_delta[i])+1
            dm[x] <- (dm[x] + v_delta[i])
            dn[x] <- dn[x] + 1
        }
        for (i in 1:(npack+1)) {
            if (dn[i] == 0)
                dm[i] <- 0
            else
                dm[i] <- round(dm[i] / dn[i])
        }
    }

    ## Create 2d proba matrix. Count element for each log and means
    ## pack.
    mx <- matrix(data = 0, nrow = npack+1, ncol = npack+1)
    for (i in 1:length(v_delta)) {
        x <- fd(v_delta[i])+1
        y <- fm(v_mean[i])+1
        mx[x,y] <- mx[x,y] + 1
    }

    ## Special cases
    pack <- unique.numeric_version(data_to_number(v_pack))
    uc <- unique.numeric_version(data_to_number(v_c))
    if (length(pack) > 1) # Global matrix, all c pack
        pack <- -1
    if (length(uc) == 1 && uc == 0) # Zero matrix, only one pack and
                                    # (c == 0)
        pack <- -2

    ## Write matrix file in mat/<name>_base_2D.txt
    file <- paste("mat/", name, "_base_2D.txt", sep = '')
    ## HEADER
    ##   Pack num
    write(x = pack, file = file, append = FALSE)
    ##   Size of matrix
    write(x = paste(npack, npack), file = file, append = TRUE)
    ##   c min and c max
    write(x = paste(min(v_c), max(v_c)), file = file, append = TRUE)
    ##   delta min and delta max
    write(x = paste(min(v_delta), max(v_delta)), file = file, append = TRUE)
    ##   Mean value for each pack
    write(x = paste(format(vfm(0:npack)), collapse = " "),
          file = file, append = TRUE)
    ##   Delta mean for each pack
    write(x = paste(format(dm), collapse = " "),
          file = file, append = TRUE)
    ## MATRIX
    ##   Matrix sep
    write(x = "[mat]", file = file, append = TRUE)
    ##   Proba matrix table
    write.table(mx, file = file, row.names = FALSE, col.names = FALSE,
                append = TRUE)

    ## Save matrix file path for simulator
    register_matrix(file, type)

    ## Make 3d plot
    ## Normalization of data
    for (x in 1:(npack+1)) {     # Deltas
        for (y in 1:(npack+1)) { # Means
            if (vx[y] != 0)
                mx[x,y] <- mx[x,y] / vx[y] # Value range mx[0:1]
        }
    }

    ## Write normalized data
    write.table(mx, file = paste("mat/", name, "_norm_2D.txt", sep = ''),
                row.names = FALSE, col.names = FALSE)

    ## Make final file for 3d plot
    mat <- matrix(data = 0, nrow = ((npack+1)*(npack+1)), ncol = 3)
    r <- 0
    for (x in 1:(npack+1)) {     # Deltas
        for (y in 1:(npack+1)) { # Means
            mat[r,1] <- x-1      #  x pos
            mat[r,2] <- y-1      #  y pos
            mat[r,3] <- mx[x,y]  #  z value
            r <- r + 1
        }
    }
    ## Write final file
    write.table(mat, file = paste("mat/", name, "_3D.txt", sep = ''),
                row.names = FALSE, col.names = FALSE)

    ## Make 3D plot
    gph <- Gpinit()
    Gpcmd(gph, "set terminal png size 800,600 enhanced font 'Helvetica,12'")
    Gpcmd(gph, paste("set output '", "png/", name, "_3D.png'", sep = ''))

    Gpcmd(gph, "set xlabel 'Delta' offset -1,0")
    Gpcmd(gph, "set zlabel 'Frequency'")
    Gpcmd(gph, "set ylabel 'Means' offset -1,0")
    Gpcmd(gph, "set title  'Delta frequency by means'")

    Gpcmd(gph, paste("set xtics ('", min(v_delta),"' 0, ",
                     "'0' ", fd(0), ", ",
                     "'", max(v_delta),"' ", npack, ")", sep = ''))
    Gpcmd(gph, paste("set ytics ('", min(v_mean),"' 0, ",
                     "'", max(v_mean),"' ", npack, ")", sep = ''))

    Gpcmd(gph, "set border 4095")
    Gpcmd(gph, "set ticslevel 0")

    Gpcmd(gph, "set datafile separator ' '")

    Gpcmd(gph, "set pm3d at s")
    Gpcmd(gph, paste("set dgrid3d ", npack+1, ",", npack+1))
    Gpcmd(gph, "set hidden3d")

    Gpcmd(gph, "set view 60,35")
    Gpcmd(gph, paste("splot './mat/", name, "_3D.txt' using 1:2:3 with lines",
                     sep = ''))

    ## Make 2D heatmaps
    Gpcmd(gph, "reset")

    Gpcmd(gph, "set terminal png size 800,600 enhanced font 'Helvetica,12'")
    Gpcmd(gph, paste("set output '", "png/", name, "_heat_map.png'",
                     sep = ''))

    Gpcmd(gph, "set xlabel 'Means' offset -1,0")
    Gpcmd(gph, "set zlabel 'Frequency'")
    Gpcmd(gph, "set ylabel 'Delta' offset -1,0")
    Gpcmd(gph, "set title  'Delta frequency by means'")

    Gpcmd(gph, paste("set ytics ('", min(v_delta),"' 0, ",
                     "'0' ", fd(0), ", ",
                     "'", max(v_delta),"' ", npack, ")", sep = ''))
    Gpcmd(gph, paste("set xtics ('", min(v_mean),"' 0, ",
                     "'", max(v_mean),"' ", npack, ")", sep = ''))

    Gpcmd(gph, "set view map")
    Gpcmd(gph, paste("splot './mat/", name,
                     "_norm_2D.txt' matrix with image", sep = ''))

    Gpcmd(gph, "reset")
    gph <- Gpclose(gph)
}
