#! /bin/sh
# Usage: ./run <dir>

STARPU_DIR=$HOME/ens/starpu/master/StarPU

# Clear old trace file
rm *.png *.txt

# Replays
for c in 0
do
    for i in 0 1 2 3 4 5 6 7 8 9
    do
        for j in 0 1 2 3 4 5 6 7 8 9
        do
            $STARPU_DIR/tools/starpu_replay      \
                --memtrace-dir $1 $PWD/tasks.rec
            # Make memorie evolution plot
            gnuplot -c  ../replay_mem_trace.gp
            mv output.png $c$i$j.png
            # Save precision of simulator
            mv precision.txt precision_$c$i$j.txt
            cat precision_$c$i$j.txt >> results.txt
        done
    done
done

# Useless gif
convert *.png annim.gif

# Display results for all simulations
../../tools/results ./results.txt

#run.sh ends here.
