set terminal png size 800,600 enhanced font 'Helvetica,12'
set output 'output.png'

set style data lines
set ylabel 'Bytes'
set xlabel 'Tasks'
set key bottom right

plot 'replay_mem_trace.dat'                                                     \
     u 1:2:($3+$1/50.) "%lf %lf %lf" w filledcurves above title 'Difference +', \
     '' u 1:2:($3+$1/50.) w filledcurves below title 'Difference -',            \
     '' u 1:2 w l title 'Simulate memory usage',                                \
     '' u 1:3 w l title 'Native memory usage'
