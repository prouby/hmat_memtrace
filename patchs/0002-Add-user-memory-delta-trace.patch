From f0e21fa87e6de831583d697a701b1c15951d742b Mon Sep 17 00:00:00 2001
From: Pierre-Antoine Rouby <pierre-antoine.rouby@ens-lyon.fr>
Date: Wed, 29 May 2019 16:16:40 +0200
Subject: [PATCH 2/8] Add user memory delta trace.

---
 include/starpu_fxt.h          |   6 ++
 src/common/fxt.c              |  27 ++++++++-
 src/common/fxt.h              |  19 +++++++
 src/debug/traces/starpu_fxt.c | 124 +++++++++++++++++++++++++++++++++++-------
 src/debug/traces/starpu_fxt.h |   1 -
 5 files changed, 154 insertions(+), 23 deletions(-)

diff --git a/include/starpu_fxt.h b/include/starpu_fxt.h
index 6fbd278..32eda70 100644
--- a/include/starpu_fxt.h
+++ b/include/starpu_fxt.h
@@ -146,6 +146,12 @@ void starpu_fxt_trace_user_event(unsigned long code);
 */
 void starpu_fxt_trace_user_event_string(const char *s);
 
+/**
+   Trace memory delta.
+ */
+void starpu_fxt_trace_memory_delta(int i, long int delta, int n, ...);
+
+
 /** @} */
 
 #ifdef __cplusplus
diff --git a/src/common/fxt.c b/src/common/fxt.c
index e28ddad..007a93c 100644
--- a/src/common/fxt.c
+++ b/src/common/fxt.c
@@ -27,6 +27,7 @@ unsigned long _starpu_job_cnt = 0;
 
 #ifdef STARPU_USE_FXT
 #include <common/fxt.h>
+#include <core/task.h>
 #include <starpu_fxt.h>
 
 #ifdef STARPU_HAVE_WINDOWS
@@ -313,7 +314,6 @@ void starpu_fxt_start_profiling()
 void starpu_fxt_stop_profiling()
 {
 }
-
 #endif // STARPU_USE_FXT
 
 void starpu_fxt_trace_user_event(unsigned long code STARPU_ATTRIBUTE_UNUSED)
@@ -329,3 +329,28 @@ void starpu_fxt_trace_user_event_string(const char *s STARPU_ATTRIBUTE_UNUSED)
 	_STARPU_TRACE_EVENT(s);
 #endif
 }
+
+
+void starpu_fxt_trace_memory_delta (int i, long int delta, int n, ...)
+{
+#ifdef STARPU_USE_FXT
+	struct starpu_task *task = starpu_task_get_current();
+
+	if (task != NULL)
+	{
+		va_list list;
+		int j;
+
+		va_start(list, n);
+
+		for (j = 0; j < n; j++)
+		{
+			_STARPU_TRACE_MEMORY_DELTA_BASE(j, va_arg(list, long int));
+		}
+
+		_STARPU_TRACE_MEMORY_DELTA(i, delta, n);
+
+		va_end(list);
+	}
+#endif
+}
diff --git a/src/common/fxt.h b/src/common/fxt.h
index a4f6ba4..d2a5a0d 100644
--- a/src/common/fxt.h
+++ b/src/common/fxt.h
@@ -237,6 +237,9 @@
 
 #define _STARPU_FUT_MEMORY_UPDATE 0x5186
 
+#define _STARPU_FUT_MEMORY_DELTA_BASE 0x5187
+#define _STARPU_FUT_MEMORY_DELTA 0x5188
+
 extern unsigned long _starpu_job_cnt;
 
 static inline unsigned long _starpu_fxt_get_job_id(void)
@@ -246,6 +249,7 @@ static inline unsigned long _starpu_fxt_get_job_id(void)
 	return ret;
 }
 
+
 #ifdef STARPU_USE_FXT
 #include <fxt/fxt.h>
 #include <fxt/fut.h>
@@ -884,6 +888,19 @@ do {										\
 				      _starpu_get_job_associated_to_task(task)->job_id); \
 	} while (0);
 
+
+#define _STARPU_TRACE_MEMORY_DELTA_BASE(i,size) do {			\
+		struct starpu_task *task = starpu_task_get_current();	\
+		if (task != NULL)					\
+			FUT_DO_PROBE3(_STARPU_FUT_MEMORY_DELTA_BASE, _starpu_get_job_associated_to_task(task)->job_id, i, size); \
+	} while (0);
+
+#define _STARPU_TRACE_MEMORY_DELTA(i,delta,n)do {			\
+		struct starpu_task *task = starpu_task_get_current();	\
+		if (task != NULL)					\
+			FUT_DO_PROBE4(_STARPU_FUT_MEMORY_DELTA, _starpu_get_job_associated_to_task(task)->job_id, i, delta, n); \
+	} while (0);
+
 #define _STARPU_TRACE_START_MEMRECLAIM(memnode,is_prefetch)		\
 	FUT_DO_PROBE3(_STARPU_FUT_START_MEMRECLAIM, memnode, is_prefetch, _starpu_gettid());
 
@@ -1216,6 +1233,8 @@ do {										\
 #define _STARPU_TRACE_END_WRITEBACK(memnode, handle)           do {(void)(memnode); (void)(handle);} while(0)
 #define _STARPU_TRACE_USED_MEM(memnode,used)		do {(void)(memnode); (void)(used);} while (0)
 #define _STARPU_TRACE_MEMORY_UPDATE(workerid, size)	do {(void)(workerid); (void)(size);} while (0)
+#define _STARPU_TRACE_MEMORY_DELTA(i,size) 		do{(void)(i); (void)(size);} while (0)
+#define _STARPU_TRACE_MEMORY_DELTA(i,delta,n) 		do{(void)(i); (void)(delta); (void)(n);} while (0)
 #define _STARPU_TRACE_START_MEMRECLAIM(memnode,is_prefetch)	do {(void)(memnode); (void)(is_prefetch);} while(0)
 #define _STARPU_TRACE_END_MEMRECLAIM(memnode,is_prefetch)	do {(void)(memnode); (void)(is_prefetch);} while(0)
 #define _STARPU_TRACE_START_WRITEBACK_ASYNC(memnode)	do {(void)(memnode);} while(0)
diff --git a/src/debug/traces/starpu_fxt.c b/src/debug/traces/starpu_fxt.c
index 54bcc85..85d0a95 100644
--- a/src/debug/traces/starpu_fxt.c
+++ b/src/debug/traces/starpu_fxt.c
@@ -34,6 +34,7 @@
 #include "starpu_fxt.h"
 #include <inttypes.h>
 #include <starpu_hash.h>
+#include <stdarg.h>
 
 #define CPUS_WORKER_COLORS_NB	8
 #define CUDA_WORKER_COLORS_NB	9
@@ -117,7 +118,11 @@ struct task_info
 	unsigned long *dependencies;
 	unsigned long ndata;
 	struct data_parameter_info *data;
-	long int size_delta;
+	long int mem_delta;
+	long int delta;
+	long int ndelta;
+	long int idelta;
+	long int *delta_base;
 	int mpi_rank;
 };
 
@@ -156,7 +161,11 @@ static struct task_info *get_task(unsigned long job_id, int mpi_rank)
 		task->dependencies = NULL;
 		task->ndata = 0;
 		task->data = NULL;
-		task->size_delta = 0;
+		task->mem_delta = 0;
+		task->delta = 0;
+		task->ndelta = 0;
+		task->idelta = 0;
+		task->delta_base = NULL;
 		task->mpi_rank = mpi_rank;
 		HASH_ADD(hh, tasks_info, job_id, sizeof(task->job_id), task);
 	}
@@ -258,11 +267,14 @@ static void task_dump(struct task_info *task, struct starpu_fxt_options *options
 		for (i = 0; i < task->ndata; i++)
 			fprintf(tasks_file, " %lu", task->data[i].size);
 		fprintf(tasks_file, "\n");
-		fprintf(tasks_file, "Size Delta:");
-		for (i = 0; i < task->ndata; i++)
-			fprintf (tasks_file, " %ld",
-				 (task->data[i].mode & STARPU_W)? task->size_delta : 0);
-		fprintf (tasks_file, "\n");
+		fprintf(tasks_file, "Mem Delta: %ld\n", task->mem_delta);
+		if (task->ndelta > 0 && task->delta_base != NULL)
+		{
+			fprintf(tasks_file, "Delta Base:");
+			for (i = 0; i < task->ndelta; i++)
+				fprintf (tasks_file, " %ld", task->delta_base[i]);
+			fprintf (tasks_file, "\n");
+		}
 	}
 
 	fprintf(tasks_file, "MPIRank: %d\n", task->mpi_rank);
@@ -270,7 +282,7 @@ static void task_dump(struct task_info *task, struct starpu_fxt_options *options
 
 	if (memtrace_file)
 	{
-		if (!((task->name == NULL) && (task->size_delta == 0)))
+		if (!((task->name == NULL) && (task->mem_delta == 0)))
 		{
 			int i, c;
 
@@ -278,28 +290,44 @@ static void task_dump(struct task_info *task, struct starpu_fxt_options *options
 				printf ("Warning: Nameless task with memory size modification.\n");
 
 			fprintf (memtrace_file, "  <mod>");
+			fprintf (memtrace_file, "<job>%lu</job>", task->job_id);
 			fprintf (memtrace_file, "<type>%s</type>", (task->name == NULL)? "Unknown" : task->name);
-			fprintf (memtrace_file, "<delta>%ld</delta>", task->size_delta);
+			fprintf (memtrace_file, "<delta>%ld</delta>", task->delta);
 
-			if (task->data)
-				for (i = 0, c = 0; i < task->ndata; i++)
+			if (task->ndelta > 0 && task->delta_base != NULL)
+				for (i = 0, c = 0; i < task->ndelta; i++)
 				{
-					if ((task->data[i].mode & STARPU_W))
-					{
-						long int v = task->data[i].size - task->size_delta;
+					long int v = task->delta_base[i];
 
+					if (i == task->idelta)
+					{
 						fprintf (memtrace_file, "<w%d>%ld</w%d><n%d>%ld</n%d>",
-							 c, (v < 0)? 0: v, c,
-							 c, task->data[i].size, c);
+							 c, v, c, c, v + task->delta, c);
 						c += 1;
 					}
-					else if ((task->data[i].mode & STARPU_R))
+					else
 					{
-						fprintf (memtrace_file, "<r%d>%ld</r%d>", c, task->data[i].size, c);
+						fprintf (memtrace_file, "<r%d>%ld</r%d>", c, v, c);
 						c += 1;
 					}
 				}
+			else if (task->data)
+				for (i = 0, c = 0; i < task->ndata; i++)
+				{
+					long int v = task->data[i].size;
 
+					if (i == task->idelta)
+					{
+						fprintf (memtrace_file, "<w%d>%ld</w%d><n%d>%ld</n%d>",
+							 c, v, c, c, v + task->mem_delta, c);
+						c += 1;
+					}
+					else
+					{
+						fprintf (memtrace_file, "<r%d>%ld</r%d>", c, v, c);
+						c += 1;
+					}
+				}
 			fprintf (memtrace_file, "</mod>\n");
 		}
 	}
@@ -312,6 +340,8 @@ static void task_dump(struct task_info *task, struct starpu_fxt_options *options
 		free(task->model_name);
 	if (task->dependencies)
 		free(task->dependencies);
+	if (task->delta_base)
+		free(task->delta_base);
 
 out:
 	HASH_DEL(tasks_info, task);
@@ -1989,7 +2019,51 @@ static void handle_memory_update (struct fxt_ev_64 *ev, struct starpu_fxt_option
 
 	struct task_info *task = get_task(job_id, options->file_rank);
 
-	task->size_delta += size;
+	task->mem_delta += size;
+}
+
+static void handle_memory_delta_base (struct fxt_ev_64 *ev, struct starpu_fxt_options *options)
+{
+	long int job_id = ev->param[0];
+	/* int data_id = ev->param[1]; */ /* FIXME: Is not necessary. */
+	long int size = ev->param[2];
+
+	int alloc = 0;
+
+	struct task_info *task = get_task(job_id, options->file_rank);
+
+	if (task->ndelta == 0)
+	{
+		alloc = 8;
+	}
+	else if (task->ndelta >= 8)
+	{
+		if (!((task->ndelta - 1) & task->ndelta)) /* Is powers of two */
+		{
+			alloc = task->ndelta * 2;
+		}
+	}
+	if (alloc)
+	{
+		_STARPU_REALLOC(task->delta_base, sizeof(long int) * alloc);
+	}
+
+	task->delta_base[task->ndelta] = size;
+	task->ndelta += 1;
+}
+
+static void handle_memory_delta (struct fxt_ev_64 *ev, struct starpu_fxt_options *options)
+{
+	long int job_id = ev->param[0];
+	int base_id = ev->param[1];
+	long int delta = ev->param[2];
+	int n = ev->param[3];
+
+	struct task_info *task = get_task(job_id, options->file_rank);
+
+	task->delta += delta;
+	task->ndelta = n;
+	task->idelta = base_id;
 }
 
 static void handle_hypervisor_end(struct fxt_ev_64 *ev, struct starpu_fxt_options *options)
@@ -2471,8 +2545,8 @@ static void handle_memnode_event(struct fxt_ev_64 *ev, struct starpu_fxt_options
 
 static void handle_memnode_event_start_3(struct fxt_ev_64 *ev, struct starpu_fxt_options *options, const char *eventstr){
 	unsigned memnode = ev->param[0];
-       unsigned size = ev->param[2];
-       unsigned long handle = ev->param[3];
+	unsigned size = ev->param[2];
+	unsigned long handle = ev->param[3];
 
 	memnode_event(get_event_time_stamp(ev, options), options->file_prefix, memnode, eventstr, handle, 0, size, memnode, options);
 }
@@ -4041,6 +4115,14 @@ void _starpu_fxt_parse_new_file(char *filename_in, struct starpu_fxt_options *op
 				handle_memory_update(&ev, options);
 				break;
 
+			case _STARPU_FUT_MEMORY_DELTA_BASE:
+				handle_memory_delta_base(&ev, options);
+				break;
+
+			case _STARPU_FUT_MEMORY_DELTA:
+				handle_memory_delta(&ev, options);
+				break;
+
 			case FUT_SETUP_CODE:
 				fut_keymask = ev.param[0];
 				break;
diff --git a/src/debug/traces/starpu_fxt.h b/src/debug/traces/starpu_fxt.h
index 2c8136a..35bc821 100644
--- a/src/debug/traces/starpu_fxt.h
+++ b/src/debug/traces/starpu_fxt.h
@@ -80,7 +80,6 @@ void _starpu_fxt_component_push(FILE *output, struct starpu_fxt_options *options
 void _starpu_fxt_component_pull(FILE *output, struct starpu_fxt_options *options, double timestamp, int workerid, uint64_t from, uint64_t to, uint64_t task, unsigned prio);
 void _starpu_fxt_component_dump(FILE *output);
 void _starpu_fxt_component_finish(FILE *output);
-
 #endif // STARPU_USE_FXT
 
 #endif // __STARPU__FXT_H__
-- 
2.7.4

