From 3048b098d85b13357eba33619c327c76621e85d2 Mon Sep 17 00:00:00 2001
From: Pierre-Antoine Rouby <pierre-antoine.rouby@ens-lyon.fr>
Date: Mon, 27 May 2019 16:37:06 +0200
Subject: [PATCH 1/8] Add FUT memory update event and memtrace xml file.

---
 include/starpu_fxt.h            |   1 +
 src/common/fxt.h                |  11 +++++
 src/datawizard/memory_manager.c |   5 ++
 src/debug/traces/starpu_fxt.c   | 104 ++++++++++++++++++++++++++++++++++++++--
 4 files changed, 117 insertions(+), 4 deletions(-)

diff --git a/include/starpu_fxt.h b/include/starpu_fxt.h
index ce7222c..6fbd278 100644
--- a/include/starpu_fxt.h
+++ b/include/starpu_fxt.h
@@ -67,6 +67,7 @@ struct starpu_fxt_options
 	char *data_path;
 	char *anim_path;
 	char *states_path;
+	char *memtrace_path;
 
 	/**
 	   In case we are going to gather multiple traces (e.g in the case of
diff --git a/src/common/fxt.h b/src/common/fxt.h
index cbaba96..a4f6ba4 100644
--- a/src/common/fxt.h
+++ b/src/common/fxt.h
@@ -235,6 +235,8 @@
 
 #define _STARPU_FUT_DATA_REQUEST_CREATED   0x5185
 
+#define _STARPU_FUT_MEMORY_UPDATE 0x5186
+
 extern unsigned long _starpu_job_cnt;
 
 static inline unsigned long _starpu_fxt_get_job_id(void)
@@ -874,6 +876,14 @@ do {										\
 #define _STARPU_TRACE_USED_MEM(memnode,used)		\
 	FUT_DO_PROBE3(_STARPU_FUT_USED_MEM, memnode, used, _starpu_gettid());
 
+#define _STARPU_TRACE_MEMORY_UPDATE(workerid, size) do {		\
+		struct starpu_task *task = starpu_task_get_current();	\
+		if (task != NULL)					\
+			FUT_DO_PROBE3(_STARPU_FUT_MEMORY_UPDATE,	\
+				      workerid, size,			\
+				      _starpu_get_job_associated_to_task(task)->job_id); \
+	} while (0);
+
 #define _STARPU_TRACE_START_MEMRECLAIM(memnode,is_prefetch)		\
 	FUT_DO_PROBE3(_STARPU_FUT_START_MEMRECLAIM, memnode, is_prefetch, _starpu_gettid());
 
@@ -1205,6 +1215,7 @@ do {										\
 #define _STARPU_TRACE_START_WRITEBACK(memnode, handle)         do {(void)(memnode); (void)(handle);} while(0)
 #define _STARPU_TRACE_END_WRITEBACK(memnode, handle)           do {(void)(memnode); (void)(handle);} while(0)
 #define _STARPU_TRACE_USED_MEM(memnode,used)		do {(void)(memnode); (void)(used);} while (0)
+#define _STARPU_TRACE_MEMORY_UPDATE(workerid, size)	do {(void)(workerid); (void)(size);} while (0)
 #define _STARPU_TRACE_START_MEMRECLAIM(memnode,is_prefetch)	do {(void)(memnode); (void)(is_prefetch);} while(0)
 #define _STARPU_TRACE_END_MEMRECLAIM(memnode,is_prefetch)	do {(void)(memnode); (void)(is_prefetch);} while(0)
 #define _STARPU_TRACE_START_WRITEBACK_ASYNC(memnode)	do {(void)(memnode);} while(0)
diff --git a/src/datawizard/memory_manager.c b/src/datawizard/memory_manager.c
index 4ffb6b8..3561333 100644
--- a/src/datawizard/memory_manager.c
+++ b/src/datawizard/memory_manager.c
@@ -125,6 +125,10 @@ int starpu_memory_allocate(unsigned node, size_t size, int flags)
 		ret = -ENOMEM;
 	}
 	STARPU_PTHREAD_MUTEX_UNLOCK(&lock_nodes[node]);
+
+	if (ret == 0)
+		_STARPU_TRACE_MEMORY_UPDATE(starpu_worker_get_id(), size);
+
 	return ret;
 }
 
@@ -145,6 +149,7 @@ void starpu_memory_deallocate(unsigned node, size_t size)
 	}
 
 	STARPU_PTHREAD_MUTEX_UNLOCK(&lock_nodes[node]);
+	_STARPU_TRACE_MEMORY_UPDATE(starpu_worker_get_id(), -size);
 }
 
 starpu_ssize_t starpu_memory_get_total(unsigned node)
diff --git a/src/debug/traces/starpu_fxt.c b/src/debug/traces/starpu_fxt.c
index 52c9bb0..54bcc85 100644
--- a/src/debug/traces/starpu_fxt.c
+++ b/src/debug/traces/starpu_fxt.c
@@ -82,6 +82,7 @@ static FILE *anim_file;
 static FILE *tasks_file;
 static FILE *data_file;
 static FILE *trace_file;
+static FILE *memtrace_file;
 
 struct data_parameter_info
 {
@@ -116,6 +117,7 @@ struct task_info
 	unsigned long *dependencies;
 	unsigned long ndata;
 	struct data_parameter_info *data;
+	long int size_delta;
 	int mpi_rank;
 };
 
@@ -154,6 +156,7 @@ static struct task_info *get_task(unsigned long job_id, int mpi_rank)
 		task->dependencies = NULL;
 		task->ndata = 0;
 		task->data = NULL;
+		task->size_delta = 0;
 		task->mpi_rank = mpi_rank;
 		HASH_ADD(hh, tasks_info, job_id, sizeof(task->job_id), task);
 	}
@@ -190,12 +193,10 @@ static void task_dump(struct task_info *task, struct starpu_fxt_options *options
 		fprintf(tasks_file, "Name: %s\n", task->name);
 		if (!task->model_name)
 			fprintf(tasks_file, "Model: %s\n", task->name);
-		free(task->name);
 	}
 	if (task->model_name)
 	{
 		fprintf(tasks_file, "Model: %s\n", task->model_name);
-		free(task->model_name);
 	}
 	fprintf(tasks_file, "JobId: %s%lu\n", prefix, task->job_id);
 	if (task->submit_order)
@@ -208,7 +209,6 @@ static void task_dump(struct task_info *task, struct starpu_fxt_options *options
 		for (i = 0; i < task->ndeps; i++)
 			fprintf(tasks_file, " %s%lu", prefix, task->dependencies[i]);
 		fprintf(tasks_file, "\n");
-		free(task->dependencies);
 	}
 	fprintf(tasks_file, "Tag: %"PRIx64"\n", task->tag);
 	if (task->workerid >= 0)
@@ -238,7 +238,6 @@ static void task_dump(struct task_info *task, struct starpu_fxt_options *options
 	if (task->parameters)
 	{
 		fprintf(tasks_file, "Parameters: %s\n", task->parameters);
-		free(task->parameters);
 	}
 	if (task->data)
 	{
@@ -259,10 +258,61 @@ static void task_dump(struct task_info *task, struct starpu_fxt_options *options
 		for (i = 0; i < task->ndata; i++)
 			fprintf(tasks_file, " %lu", task->data[i].size);
 		fprintf(tasks_file, "\n");
+		fprintf(tasks_file, "Size Delta:");
+		for (i = 0; i < task->ndata; i++)
+			fprintf (tasks_file, " %ld",
+				 (task->data[i].mode & STARPU_W)? task->size_delta : 0);
+		fprintf (tasks_file, "\n");
 	}
+
 	fprintf(tasks_file, "MPIRank: %d\n", task->mpi_rank);
 	fprintf(tasks_file, "\n");
 
+	if (memtrace_file)
+	{
+		if (!((task->name == NULL) && (task->size_delta == 0)))
+		{
+			int i, c;
+
+			if (task->name == NULL)
+				printf ("Warning: Nameless task with memory size modification.\n");
+
+			fprintf (memtrace_file, "  <mod>");
+			fprintf (memtrace_file, "<type>%s</type>", (task->name == NULL)? "Unknown" : task->name);
+			fprintf (memtrace_file, "<delta>%ld</delta>", task->size_delta);
+
+			if (task->data)
+				for (i = 0, c = 0; i < task->ndata; i++)
+				{
+					if ((task->data[i].mode & STARPU_W))
+					{
+						long int v = task->data[i].size - task->size_delta;
+
+						fprintf (memtrace_file, "<w%d>%ld</w%d><n%d>%ld</n%d>",
+							 c, (v < 0)? 0: v, c,
+							 c, task->data[i].size, c);
+						c += 1;
+					}
+					else if ((task->data[i].mode & STARPU_R))
+					{
+						fprintf (memtrace_file, "<r%d>%ld</r%d>", c, task->data[i].size, c);
+						c += 1;
+					}
+				}
+
+			fprintf (memtrace_file, "</mod>\n");
+		}
+	}
+
+	if (task->name)
+		free(task->name);
+	if (task->parameters)
+		free(task->parameters);
+	if (task->model_name)
+		free(task->model_name);
+	if (task->dependencies)
+		free(task->dependencies);
+
 out:
 	HASH_DEL(tasks_info, task);
 	free(task);
@@ -1931,6 +1981,17 @@ static void handle_hypervisor_begin(struct fxt_ev_64 *ev, struct starpu_fxt_opti
 	}
 }
 
+static void handle_memory_update (struct fxt_ev_64 *ev, struct starpu_fxt_options *options)
+{
+	/* int workerid = ev->param[0]; */
+	long int size = ev->param[1];
+	int job_id = ev->param[2];
+
+	struct task_info *task = get_task(job_id, options->file_rank);
+
+	task->size_delta += size;
+}
+
 static void handle_hypervisor_end(struct fxt_ev_64 *ev, struct starpu_fxt_options *options)
 {
 	int worker;
@@ -3976,6 +4037,10 @@ void _starpu_fxt_parse_new_file(char *filename_in, struct starpu_fxt_options *op
 				handle_hypervisor_end(&ev, options);
 				break;
 
+			case _STARPU_FUT_MEMORY_UPDATE:
+				handle_memory_update(&ev, options);
+				break;
+
 			case FUT_SETUP_CODE:
 				fut_keymask = ev.param[0];
 				break;
@@ -4103,6 +4168,7 @@ void starpu_fxt_options_init(struct starpu_fxt_options *options)
 	options->data_path = "data.rec";
 	options->anim_path = "trace.html";
 	options->states_path = "trace.rec";
+	options->memtrace_path = "memtrace.xml";
 	options->distrib_time_path = "distrib.data";
 	options->dumped_codelets = NULL;
 	options->activity_path = "activity.data";
@@ -4191,6 +4257,12 @@ void _starpu_fxt_write_trace_header(FILE *f)
 }
 
 static
+void _starpu_fxt_write_memtrace_header(FILE *f)
+{
+	fprintf(f, "<mem_trace>\n");
+}
+
+static
 void _starpu_fxt_trace_file_init(struct starpu_fxt_options *options)
 {
 	if (options->states_path)
@@ -4203,6 +4275,18 @@ void _starpu_fxt_trace_file_init(struct starpu_fxt_options *options)
 }
 
 static
+void _starpu_fxt_memtrace_file_init(struct starpu_fxt_options *options)
+{
+	if (options->memtrace_path)
+		memtrace_file = fopen(options->memtrace_path, "w+");
+	else
+		memtrace_file = NULL;
+
+	if (memtrace_file)
+		_starpu_fxt_write_memtrace_header(memtrace_file);
+}
+
+static
 void _starpu_fxt_activity_file_close(void)
 {
 	if (activity_file)
@@ -4242,6 +4326,16 @@ void _starpu_fxt_trace_file_close(void)
 }
 
 static
+void _starpu_fxt_memtrace_file_close(void)
+{
+	if (memtrace_file)
+	{
+		fprintf(memtrace_file, "</mem_trace>");
+		fclose(memtrace_file);
+	}
+}
+
+static
 void _starpu_fxt_paje_file_init(struct starpu_fxt_options *options)
 {
 	/* create a new file */
@@ -4335,6 +4429,7 @@ void starpu_fxt_generate_trace(struct starpu_fxt_options *options)
 	_starpu_fxt_tasks_file_init(options);
 	_starpu_fxt_data_file_init(options);
 	_starpu_fxt_trace_file_init(options);
+	_starpu_fxt_memtrace_file_init(options);
 
 	_starpu_fxt_paje_file_init(options);
 
@@ -4465,6 +4560,7 @@ void starpu_fxt_generate_trace(struct starpu_fxt_options *options)
 	_starpu_fxt_tasks_file_close();
 	_starpu_fxt_data_file_close();
 	_starpu_fxt_trace_file_close();
+	_starpu_fxt_memtrace_file_close();
 
 	_starpu_fxt_dag_terminate();
 
-- 
2.7.4

