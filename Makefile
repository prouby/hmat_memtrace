# Makefile

R = Rscript
SCRIPT = memtrace.r

EXAMPLE = memtrace.xml
SIMU = memtrace.xml

001 = memtrace-0.01.xml
002 = memtrace-0.02.xml
005 = memtrace-0.05.xml

LOG_FILE = run.log

all: tools compile run

tools:
	@echo " TOOLS"
	@make -C tools

compile: $(SCRIPT)
	@echo " COMPILE -- Noting to do"

run: clean_local run_5 run_2 run_1
	@echo " DONE"

run_example:
	@echo " RUN $(EXAMPLE)"
	@cd xml-example/ &&			\
		$(R) ../$(SCRIPT)		\
			$(EXAMPLE) &&		\
		cd ..

run_simu:
	@echo " RUN $(SIMU)"
	@cd xml-simu/ &&			\
		$(R) ../$(SCRIPT)		\
			$(SIMU) &&		\
		cd ..

run_5:
	@echo " RUN 0.05"
	@cd xml-0_05/ &&				\
		$(R) ../$(SCRIPT)			\
			$(005) >> ../$(LOG_FILE) &&	\
		cd ..

run_2:
	@echo " RUN 0.02"
	@cd xml-0_02/ &&				\
		$(R) ../$(SCRIPT) --no-fxt		\
			$(002) >> ../$(LOG_FILE) &&	\
		cd ..

run_1:
	@echo " RUN 0.01"
	@cd xml-0_01/ &&				\
		$(R) ../$(SCRIPT) --no-fxt		\
			$(001) >> ../$(LOG_FILE) &&	\
		cd ..

run_sphere:
	@echo " RUN sphere"
	@cd xml-sphere/ &&						\
		$(R) ../$(SCRIPT)					\
			../xml-sphere/memtrace.xml >> ../$(LOG_FILE) &&	\
		cd ..

simulation:
	@echo " SIMU"
	@make -C simu/ all

clean: clean_local
	@echo " CLEAN"
	@make -C tools/ clean
	@make -C simu/ clean

clean_local:
	@rm -rf run.log
	@rm -rf xml-*/png
	@rm -rf xml-*/csv
	@rm -rf xml-*/bin
	@rm -rf xml-*/mat


.PHONY: compile run clean run_1 run_2 run_5 tools

# Makefile ends here.
