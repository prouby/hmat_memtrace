##########################
## * Global variables * ##
##########################


graph_size <- 800


dir.create ("png" ,showWarnings = FALSE)
dir.create ("csv" ,showWarnings = FALSE)
dir.create ("mat" ,showWarnings = FALSE)

## Init Matrix file with matrix block number
init_matrix_list <- function (type) {
    write("matrix", file = paste("mat/", type, "_matrix.txt"), append=FALSE)
}
