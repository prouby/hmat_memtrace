#! /usr/bin/Rscript
## memtrace.r --- Memtrace parser.  -*- mode: ess-r -*-

#################
## * Library * ##
#################
library(XML)
library(Rgnuplot)

## Some function as defined on additionals sources files
sub <- ".."
if (file.exists("config.r"))
    sub <- "."
source(paste(sub, "config.r", sep = '/'))
source(paste(sub, "gnuplot.r", sep = '/'))
source(paste(sub, "graph.r", sep = '/'))
source(paste(sub, "pack.r", sep = '/'))
source(paste(sub, "fxt.r", sep = '/'))

###################
## * Functions * ##
###################

init_matrix_list <- function (type) {
    ## Init Matrix list file.
    write("matrix",
          file = paste("mat/", type, "_matrix.txt", sep = ''),
          append=FALSE)
}

register_matrix <- function(path, type) {
    # Register matrix path on matrix list file.
    write(path,
          file = paste("mat/", type, "_matrix.txt", sep = ''),
          append=TRUE)
}

mean.harmonic <- function (a, b) {
    ## Means (https://en.wikipedia.org/wiki/Harmonic_mean)
    ## v_mean  <- ((v_a + v_b) / 2) # Arithmetic mean
    ## v_mean  <- 2 / ((1 / v_a) + (1 / v_b)) # Harmonic mean
    if (a+b == 0)
        return (0)
    return (round((2 * a * b) / (a + b)))
}

data_to_number <- function (data) {
    ## Dataframe column to numeric vector.
    ##   ```data_to_number (data[,"c"])```
    return (as.numeric (as.vector (data)))
}

abcn_with_max_KB_dataframe <- function (data) {
    ## New data frame with value as KBytes. Values ordered by (c, new,
    ## a, b).
    v_a    <- data_to_number (data[,"a"])
    v_b    <- data_to_number (data[,"b"])
    v_c    <- data_to_number (data[,"c"])
    v_amax <- data_to_number (data[,"amax"])
    v_bmax <- data_to_number (data[,"bmax"])
    v_cmax <- data_to_number (data[,"cmax"])
    v_new  <- data_to_number (data[,"new"])

    k_a    <- mapply (B_to_kB, v_a)
    k_b    <- mapply (B_to_kB, v_b)
    k_c    <- mapply (B_to_kB, v_c)
    k_amax <- mapply (B_to_kB, v_amax)
    k_bmax <- mapply (B_to_kB, v_bmax)
    k_cmax <- mapply (B_to_kB, v_cmax)
    k_new  <- mapply (B_to_kB, v_new)

    newdata <- data.frame (a = k_a,
                           b = k_b,
                           c = k_c,
                           amax = k_amax,
                           bmax = k_bmax,
                           cmax = k_cmax,
                           new  = k_new)

    s_newdata <- newdata[with (newdata, order (c, new, a, b)), ]

    return (s_newdata)
}

abcn_KB_dataframe <- function (data) {
    ## New data frame with value as KBytes. Values ordered by (c, new,
    ## a, b).
    v_a    <- data_to_number (data[,"a"])
    v_b    <- data_to_number (data[,"b"])
    v_c    <- data_to_number (data[,"c"])
    v_new  <- data_to_number (data[,"new"])

    k_a    <- mapply (B_to_kB, v_a)
    k_b    <- mapply (B_to_kB, v_b)
    k_c    <- mapply (B_to_kB, v_c)
    k_new  <- mapply (B_to_kB, v_new)

    newdata <- data.frame (a = k_a,
                           b = k_b,
                           c = k_c,
                           new  = k_new)

    s_newdata <- newdata[with (newdata, order (c, new, a, b)), ]

    return (s_newdata)
}

abcn_rebuild_dataframe <- function (data) {
    ## Get (a, b, c, new) DATA and return new dataframe with modified
    ## data. New dataframe containe a, b, c, new, delta, sum and mean
    ## on KiloByte.
    data <- abcn_with_max_KB_dataframe (data) # Data as KiloBytes

    v_a    <- data_to_number (data[,"a"])
    v_b    <- data_to_number (data[,"b"])
    v_c    <- data_to_number (data[,"c"])
    v_amax <- data_to_number (data[,"amax"])
    v_bmax <- data_to_number (data[,"bmax"])
    v_cmax <- data_to_number (data[,"cmax"])
    v_new  <- data_to_number (data[,"new"])

    ## Make 15 pack with same number of data
    pack <- make_len_pack_lambda (length(v_c), n = 15)

    ## Register data index
    v_x <- 0:(length (v_c) - 1)

    ## Define data pack from data index
    v_p <- mapply (pack, v_x)

    ## Compute delta (newc - oldc)
    v_delta <- mapply (function (x,y) x-y, v_new, v_c)

    ## Sum a and b (FIXME: Usefull?)
    v_sum   <- v_a + v_b ## + v_c

    ## Harmonic mean a, b
    v_mean  <- mean.harmonic(v_a, v_b)

    ## Make log pack for means
    f_log <- make_log_pack_lambda(min(v_mean), max(v_mean))
    v_log <- mapply (f_log, v_mean)

    ## Make extended new dataframe
    newdata <- data.frame (pack  = v_p,
                           a     = v_a,
                           b     = v_b,
                           c     = v_c,
                           amax  = v_amax,
                           bmax  = v_bmax,
                           cmax  = v_cmax,
                           x     = v_x,
                           new   = v_new,
                           delta = v_delta,
                           sum   = v_sum,
                           mean  = v_mean,
                           log   = v_log)

    ## Sort data by pack, c, new, a, b and delta
    s_newdata <- newdata[with (newdata,
                               order (pack,
                                      c, new,
                                      a, b, delta)), ]

    return (s_newdata)
}

cn_rebuild_dataframe <- function (data) {
    ## Same function that 'abcn_rebuild_dataframe' but use only c and
    ## new datas.
    data <- abcn_KB_dataframe (data)

    v_c <- data_to_number (data[,"c"])
    v_new <- data_to_number (data[,"new"])

    ## Make pack of c with same data number
    pack    <- make_len_pack_lambda (length (v_c), n = 15)
    v_x     <- 1:length (v_c)
    v_p     <- mapply (pack, v_x)
    v_delta <- mapply (function (x,y) x-y, v_new, v_c)

    ## Make extended new dataframe
    newdata <- data.frame (c     = v_c,
                           x     = v_x,
                           pack  = v_p,
                           new   = v_new,
                           delta = v_delta)

    ## Sort data
    s_newdata <- newdata[with (newdata,
                               order (pack, c, new, delta)), ]

    return (s_newdata)
}

bcn_rebuild_dataframe <- function (data) {
    ## Same function that 'abcn_rebuild_dataframe' but use only c and
    ## new datas.
    data <- abcn_KB_dataframe (data)

    v_b <- data_to_number (data[,"b"])
    v_c <- data_to_number (data[,"c"])
    v_new <- data_to_number (data[,"new"])

    ## Make pack of c with same data number
    pack <- make_len_pack_lambda(length (v_c), n = 15)
    v_x <- 1:length (v_c)
    v_p     <- mapply (pack, v_x)
    v_delta <- mapply (function (x,y) x-y, v_new, v_c)

    ## Make extended new dataframe
    newdata <- data.frame (b     = v_b,
                           c     = v_c,
                           x     = v_x,
                           pack  = v_p,
                           new   = v_new,
                           delta = v_delta)

    ## Sort data
    s_newdata <- newdata[with (newdata,
                               order (pack, c, new, b, delta)), ]

    return (s_newdata)
}

generic_make_graphs_foreach_pack <- function (data, type, f) {
    ## Make graph for each value of 'c' and save this each data on csv
    ## file. F is function to make graph.
    v <- unique.numeric_version  (data_to_number (data[,"pack"]))

    ## For each pack
    for (vi in v) {
        sub_data <- subset (data, (pack %in% vi))

        name <- paste (type, "pack", vi, sep="_")
        f (data = sub_data,
           name = name,
           dtype = type,
           type = "l",
           title = paste (
               type, "memory for pack of data", vi),
           csv = TRUE, summary = FALSE)
    }
}

generic_make_graphs_foreach_log_pack <- function (data, type, f) {
    ## Make graph for each value of 'c' and save this each data on csv
    ## file. F is function to make graph.
    v <- unique.numeric_version  (data_to_number (data[,"log"]))

    ## For each log pack
    for (vi in v) {
        sub_data <- subset (data, (log %in% vi))

        name <- paste (type, "log_pack", vi, sep="_")
        f (data = sub_data,
           name = name,
           dtype = type,
           type = "l",
           title = paste (
               type, "memory for log mean pack: ", vi),
           histo = FALSE,
           csv = FALSE, summary = FALSE)
    }
}

##############
## * Main * ##
##############

t <- proc.time ()

usage <- function (args) {
    print ("Usage: Rscript memtrace.r [OPTIONS] [xml_trace_file...]")
    print ("  Options:")
    print ("     --no-fxt    Read old XML file format.")
}

## Parsing args
args <- commandArgs (trailingOnly = TRUE)
nargs <- 1
if ("--help" %in% args) {
    usage ()
    quit ()
}
if (length (args) < 1) {
    usage ()
    quit ()
}

## Use FxT format
fxt <- TRUE
if ("--no-fxt" %in% args) {
    fxt <- FALSE
    nargs <- nargs + 1
}

## Read all xml traces passed on arguments
mydata <- data.frame()
for (i in nargs:length(args)) {
    xml_file <- args[length(args)]

    print ("Read XML ...")
    mydata <- rbind(mydata, xmlToDataFrame(xml_file))
    print ("Done")
}

if (fxt == TRUE) {
    ## StarPU+FXT version use different trace format.
    gemm <- subset (mydata, type %in% "GemmLeaf")
    trsm <- subset (mydata, type %in% "SolveUpperTriangularRig")
    ldlt <- subset (mydata, type %in% "LDLt")
    mdmt <- subset (mydata, type %in% "MDMt")

    gemm <- fxt_gemm_rebuild_dataframe (gemm)
    trsm <- fxt_trsm_rebuild_dataframe (trsm)
    ldlt <- fxt_ldlt_rebuild_dataframe (ldlt)
    mdmt <- fxt_mdmt_rebuild_dataframe (mdmt)
} else {
    gemm <- subset (mydata, type %in% "GEMM")
    trsm <- subset (mydata, type %in% "TRSM")
    ldlt <- subset (mydata, type %in% "LDLT")
    mdmt <- subset (mydata, type %in% "MDMT")
}

## Init proba matrix list (use by simulator)
init_matrix_list("GemmLeaf")
init_matrix_list("SolveUpperTriangularRig")
init_matrix_list("LDLt")
init_matrix_list("MDMt")

##########
## GEMM ##
##########
print ("Process GEMM data...")

## Split GEMM on to dataframe, when (c == 0) and when (c != 0)
r0 <- abcn_rebuild_dataframe (subset (gemm, c %in% 0))
rest <- abcn_rebuild_dataframe (subset (gemm, !(c %in% 0)))

## Make graph for all data
## gemm_make_graph (data = abcn_rebuild_dataframe(gemm),
##                  name = "GemmLeaf_all",
##                  dtype = "GemmLeaf",
##                  type = "l",
##                  title = "GemmLeaf memory all data view",
##                  csv = TRUE, summary = FALSE)

## Make graph for data when (c == 0)
gemm_make_graph (data = r0,
                 name = "GemmLeaf_0",
                 dtype = "GemmLeaf",
                 type = "l",
                 title = "GemmLeaf memory global view",
                 csv = TRUE, summary = FALSE, zero = TRUE)

## Make graphs for each pack of data when (c != 0)
generic_make_graphs_foreach_pack (rest, "GemmLeaf", gemm_make_graph)

## Make graphs for data when (c != 0)
gemm_make_graph (data = rest,
                 name = "GemmLeaf_global",
                 dtype = "GemmLeaf",
                 type = "l",
                 title = "GemmLeaf memory global view",
                 csv = TRUE, summary = FALSE)

print ("Done")

##########
## LDLt ##
##########
print ("Process LDLt data ...")

rest <- cn_rebuild_dataframe (subset (ldlt, !(c %in% 0)))

generic_make_graphs_foreach_pack (rest, "LDLt", ldlt_make_graph)

ldlt_make_graph (data = rest,
                 name = "LDLt_global",
                 dtype = "LDLt",
                 type = "l",
                 title = "LDLt memory global view",
                 csv = TRUE, summary = FALSE)

print ("Done.")

##########
## MDMt ##
##########
print ("Process MDMt data ...")

rest <- abcn_rebuild_dataframe (subset (mdmt, !(c %in% 0)))

generic_make_graphs_foreach_pack (rest, "MDMt", mdmt_make_graph)

mdmt_make_graph (data = rest,
                 name = "MDMt_global",
                 dtype = "MDMt",
                 type = "l",
                 title = "MDMt memory global view",
                 csv = TRUE, summary = FALSE)

print ("Done.")

#############################
## SolveUpperTriangularRig ##
#############################
print ("Process SolveUpperTriangularRig data ...")

rest <- bcn_rebuild_dataframe (subset (trsm, !(c %in% 0)))

generic_make_graphs_foreach_pack (rest, "SolveUpperTriangularRig", trsm_make_graph)

trsm_make_graph (data = rest,
                 name = "SolveUpperTriangularRig_global",
                 dtype = "SolveUpperTriangularRig",
                 type = "l",
                 title = "SolveUpperTriangularRig memory global view",
                 csv = TRUE, summary = FALSE)

print ("Done.")

## Time of script
print (proc.time () - t)

## memtrace.r ends here.
