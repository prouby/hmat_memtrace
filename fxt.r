#############
## * FxT * ##
#############

## Function on this file is used to 'translate' fxt trace dataframe to
## classique dataframe.

## TODO Add data max size for LDLt, MDMt, TRSM

fxt_gemm_rebuild_dataframe <- function (data) {
    ## Writable data
    w0 <- data_to_number (data[,"w0"])
    w0max <- data_to_number (data[,"w0max"])

    n0 <- data_to_number (data[,"n0"])
    ## Readable data
    r1 <- data_to_number (data[,"r1"])
    r2 <- data_to_number (data[,"r2"])

    r1max <- data_to_number (data[,"r1max"])
    r2max <- data_to_number (data[,"r2max"])

    ## Delta
    delta <- data_to_number (data[,"delta"])

    newdata <- data.frame (a     = r1,
                           amax  = r1max,
                           b     = r2,
                           bmax  = r2max,
                           c     = w0,
                           cmax  = w0max,
                           new   = n0,
                           delta = delta)
    return (newdata)
}

fxt_ldlt_rebuild_dataframe <- function (data) {
    ## Writable data
    w0 <- data_to_number (data[,"w0"])
    n0 <- data_to_number (data[,"n0"])
    ## Delta
    delta <- data_to_number (data[,"delta"])

    ## Dummy data
    d <- mapply (function (x) 0, c(1:length(delta)))

    newdata <- data.frame (a     = d,
                           b     = d,
                           c     = w0,
                           new   = n0,
                           delta = delta)
    return (newdata)
}

fxt_mdmt_rebuild_dataframe <- function (data) {
    ## Writable data
    w0 <- data_to_number (data[,"w0"])
    n0 <- data_to_number (data[,"n0"])
    w0max <- data_to_number (data[,"w0max"])

    ## Readable data
    r1 <- data_to_number (data[,"r1"])
    r2 <- data_to_number (data[,"r2"])
    r1max <- data_to_number (data[,"r1max"])
    r2max <- data_to_number (data[,"r2max"])
    ## Delta
    delta <- data_to_number (data[,"delta"])

    newdata <- data.frame (a     = r1,
                           b     = r2,
                           c     = w0,
                           amax  = r1max,
                           bmax  = r2max,
                           cmax  = w0max,
                           new   = n0,
                           delta = delta)
    return (newdata)
}

fxt_trsm_rebuild_dataframe <- function (data) {
    ## Writable data
    w0 <- data_to_number (data[,"w0"])
    n0 <- data_to_number (data[,"n0"])
    ## Readable data
    r1 <- data_to_number (data[,"r1"])
    ## Delta
    delta <- data_to_number (data[,"delta"])

    ## Dummy data
    d <- mapply (function (x) 0, c(1:length(delta)))

    newdata <- data.frame (a     = d,
                           b     = r1,
                           c     = w0,
                           new   = n0,
                           delta = delta)
    return (newdata)
}
