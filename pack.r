#################
## * Packing * ##
#################

make_len_pack_lambda <- function (len, n = 15) {
    ## Return function to pack data by number of data. Each pack as
    ## the same number of data.
    pack_size <- len / n
    pack <- function (x) {
        return (round (x / pack_size))
    }
    return (pack)
}

make_log_pack_lambda <- function (min, max, n = 15) {
    ## Return function to pack data with log scale.

    ## TODO: Use the reverse equation instead of loop.
    diff <- (log (max / min) / n)
    pack <- function (x) {
        ## Find pack corresponding to X value.
        for (i in c(0:n)) {
            if (x <= (round (min * (exp (i * diff)))))
                return (i)
        }
    }
    return (pack)
}

get_value_log_pack <- function (min, max, n = 15) {
    ## Retrurn function to get value for X logarithm pack
    diff <- (log (max / min) / n)
    f <- function (x) {
        return (round (min * (exp (x * diff))))
    }
    return (f)
}

make_pack_lambda <- function (vmin, vmax, n = 15) {
    ## Return function to pack data.
    add <- -vmin

    diff <- vmax - vmin
    size <- diff / n

    f <- function (x) {
        round((x + add) / size)
    }
    return (f)
}

get_value_pack <- function (vmin, vmax, n = 15) {
    ## Retrun function to get minimal value for X pack.
    add <- -vmin

    diff <- vmax - vmin
    size <- diff / n

    f <- function (x) {
        return (round(x * size)-add)
    }
    return (f)
}

## Return function to get value rounded as KiloBytes
B_to_kB <- function (x) round (x/1000)
