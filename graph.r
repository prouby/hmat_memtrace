######################
## * Graph & Hist * ##
######################

make_histogram <- function (data, name, normalized = FALSE) {
    ## Make histograme for each delta pack of values on DATA. Save
    ## histo on 'NAME_histo.png'.
    v_delta <- data_to_number (data[,"delta"])

    ## Number of pack
    npack = 30

    ## Create function for data pack
    fd <- make_pack_lambda(min(v_delta), max(v_delta), n = npack)

    ## No data
    if (length(v_delta) == 0)
        return (0);

    ## Count all delta for each pack
    vd <- mapply(function(x) 0, 1:(npack+1))
    for (i in 1:length(v_delta)) {
        x <- fd(v_delta[i])+1 # +1 because index start at 1
        vd[x] <- vd[x] + 1    # Inc num of delta
    }

    ## Write result on tmp file
    file <- "tmp.txt"
    write(x = "", file = file, append = FALSE)
    for (i in 1:(npack+1)) {
        write(x = vd[i], file = file, append = TRUE)
    }

    ## Make histogram with gnuplot
    gph <- Gpinit()
    Gpcmd(gph, "set terminal png size 800,600 enhanced font 'Helvetica,12'")
    Gpcmd(gph, paste("set output 'png/", name, "_histo.png'", sep = ''))

    Gpcmd(gph, paste("set xrange [0:", npack, "]", sep = ''))

    Gpcmd(gph, "set xlabel 'Delta'")
    Gpcmd(gph, "set ylabel 'Frequency'")
    Gpcmd(gph, "set title  'Delta frequency'")
    Gpcmd(gph, "set logscale y") # Logscale

    Gpcmd(gph, paste("set xtics ('", min(v_delta),"' 0, ",
                     "'0' ", fd(0), ", ",
                     "'", max(v_delta),"' ", npack, ")", sep = ''))

    Gpcmd(gph, "set style data histogram")
    Gpcmd(gph, "set style histogram cluster gap 0")
    Gpcmd(gph, "set style fill solid border -1")

    Gpcmd(gph, paste("plot './tmp.txt'", sep = ''))

    Gpcmd(gph, "reset")
    gph <- Gpclose(gph)

    ## ## Old histogram method, impossible to make log scale?
    ## png (file = paste ("png/", name, "_histo", ".png", sep = ""),
    ##      width=graph_size, height=graph_size)

    ## hist(x = v_delta, xlab = "Size diff (new_c - old_c)",
    ##      col = "red", border = "black", breaks = 50,
    ##      probability = FALSE)

    ## dev.off ()
}

gemm_make_graph <- function (data, name, dtype, type, title, csv, summary,
                             histo = TRUE, zero = FALSE) {
    ## Make graph for DATA and save it in 'NAME.png'.  TYPE is type of
    ## data representation on graph, like "o", "l" or "p". TITLE is
    ## the title of graph. CSV, and SUMMARY is boolean for saving
    ## or not DATA on files.

    v_a     <- data_to_number (data[,"a"])
    v_b     <- data_to_number (data[,"b"])
    v_c     <- data_to_number (data[,"c"])
    v_amax  <- data_to_number (data[,"amax"])
    v_bmax  <- data_to_number (data[,"bmax"])
    v_cmax  <- data_to_number (data[,"cmax"])
    v_p     <- data_to_number (data[,"pack"])
    v_sum   <- data_to_number (data[,"sum"])
    v_new   <- data_to_number (data[,"new"])
    v_delta <- data_to_number (data[,"delta"])
    v_mean  <- data_to_number (data[,"mean"])
    v_log   <- data_to_number (data[,"log"])

    v_rank  <- v_c/v_cmax

    data <- data.frame (a = v_a,
                        b = v_b,
                        c = v_c,
                        amax = v_amax,
                        bmax = v_bmax,
                        cmax = v_cmax,
                        pack = v_p,
                        sum = v_sum,
                        new = v_new,
                        delta = v_delta,
                        mean = v_mean,
                        log = v_log,
                        rank = v_rank)

    data <- data[with (data,
                       order (rank, pack, c, new, b, delta)), ]


    v_a     <- data_to_number (data[,"a"])
    v_b     <- data_to_number (data[,"b"])
    v_c     <- data_to_number (data[,"c"])
    v_amax  <- data_to_number (data[,"amax"])
    v_bmax  <- data_to_number (data[,"bmax"])
    v_cmax  <- data_to_number (data[,"cmax"])
    v_p     <- data_to_number (data[,"pack"])
    v_sum   <- data_to_number (data[,"sum"])
    v_new   <- data_to_number (data[,"new"])
    v_delta <- data_to_number (data[,"delta"])
    v_mean  <- data_to_number (data[,"mean"])
    v_log   <- data_to_number (data[,"log"])
    v_rank  <- data_to_number (data[,"rank"])

    ## Make 3d plot for DATA.
    gnuplot_3d_lines(data, type = dtype, name = name)

    ## None representative data
    if (length (v_p) < 5)
        return ()

    ## Save data as CSV
    if (csv) {
        write.csv (data, paste ("csv/", name, ".csv", sep = ""),
                   row.names = FALSE)
    }

    ## Make data summary and save it as CSV
    if (summary) {
        write.csv (abc_data_summary (data),
                   paste ("csv/", name, "_summary", ".csv", sep = ""),
                   row.names = FALSE)
    }

    ## Make histogram for data
    if (histo) {
        make_histogram (data, name)
    }

    ## Make data lines plot
    png (file = paste ("png/", name, ".png", sep = ""),
         width=graph_size, height=graph_size)

    plot (x = v_new,
          ## log = "x",
          xlim = c(0, length(v_new)),
          ylim = c(min(v_a, v_b, v_c, v_new),
                   max(v_mean, v_c, v_new)),
          type = type, col = "red",
          xlab = "Data", ylab = "Size (KBytes)",
          main = title)

    lines (v_mean, type = type, col = "purple")
    lines (v_new,  type = type, col = "red")
    lines (v_c,    type = type, col = "blue")
    lines (v_rank, type = type, col = "yellow")

    legend ('topleft', lty=1, bty = 'n', cex=.75,
            legend = c("New (c)", "Old (c)",  "Harmonic mean (a,b)"),
            col    = c("red", "blue", "purple"))

    dev.off ()

    png (file = paste ("png/", name, "_max.png", sep = ""),
         width=graph_size, height=graph_size)

    if (!zero) {
        plot (x = v_rank, log = "y", type = type, col = "red", main = title)
        dev.off ()
    }
}

ldlt_make_graph <- function (data, name, dtype, type, title, csv, summary) {
    ## Make graph for DATA and save it in 'NAME.png'.  TYPE is type of
    ## data representation on graph, like "o", "l" or "p". TITLE is
    ## the title of graph. CSV and SUMMARY is boolean for saving
    ## or not DATA on files.
    v_c     <- data_to_number (data[,"c"])
    v_p     <- data_to_number (data[,"pack"])
    v_new   <- data_to_number (data[,"new"])
    v_delta <- data_to_number (data[,"delta"])

    ## FIXME: Some of data is missing to do this 3d plot
    ## gnuplot_3d_lines(data, type = dtype, name = name, mean = FALSE)

    ## None representative data
    if (length (v_p) < 5)
        return ()

    ## Save data as CSV
    if (csv) {
        write.csv (data, paste ("csv/", name, ".csv", sep = ""),
                   row.names = FALSE)
    }


    make_histogram (data, name)

    ## Plot
    png (file = paste ("png/", name, ".png", sep = ""),
         width=graph_size, height=graph_size)

    plot (x = v_new,
          xlim = c(0, length(v_p)),
          ylim = c(min(v_c, v_new),
                   max(v_c, v_new)),
          type = type, col = "red",
          xlab = "Data", ylab = "Size (KBytes)",
          main = title)

    lines (v_new,  type = type, col = "red")
    lines (v_c,    type = type, col = "blue")

    legend ('topleft', lty=1, bty = 'n', cex=.75,
            legend = c("New (c)", "Old (c)"),
            col    = c("red", "blue"))

    dev.off ()
}

mdmt_make_graph <- function (data, name, dtype, type, title, csv, summary) {
    ## Make graph for DATA and save it in 'NAME.png'.  TYPE is type of
    ## data representation on graph, like "o", "l" or "p". TITLE is
    ## the title of graph. CSV and SUMMARY is boolean for saving
    ## or not DATA on files.
    v_a     <- data_to_number (data[,"a"])
    v_b     <- data_to_number (data[,"b"])
    v_c     <- data_to_number (data[,"c"])
    v_p     <- data_to_number (data[,"pack"])
    v_sum   <- data_to_number (data[,"sum"])
    v_new   <- data_to_number (data[,"new"])
    v_delta <- data_to_number (data[,"delta"])
    v_mean  <- data_to_number (data[,"mean"])

    ## FIXME
    ## gnuplot_3d_lines(data, type = dtype, name = name, mean = FALSE)

    ## None representative data
    if (length (v_p) < 5)
        return ()

    if (csv) {
        write.csv (data, paste ("csv/", name, ".csv", sep = ""),
                   row.names = FALSE)
    }

    if (summary) {
        write.csv (abc_data_summary (data),
                   paste ("csv/", name, "_summary", ".csv", sep = ""),
                   row.names = FALSE)
    }

    make_histogram (data, name)

    ## Plot
    png (file = paste ("png/", name, ".png", sep = ""),
         width=graph_size, height=graph_size)

    plot (x = v_new,
          xlim = c(0, length(v_p)),
          ylim = c(min(v_a, v_b, v_c, v_new),
                   max(v_mean, v_c, v_new)),
          type = type, col = "red",
          xlab = "Data", ylab = "Size (KBytes)",
          main = title)

    lines (v_mean, type = type, col = "purple")
    lines (v_new,  type = type, col = "red")
    lines (v_c,    type = type, col = "blue")

    legend ('topleft', lty=1, bty = 'n', cex=.75,
            legend = c("New (c)", "Old (c)",  "Harmonic mean (a,b)"),
            col    = c("red", "blue", "purple"))

    dev.off ()
}

trsm_make_graph <- function (data, name, dtype, type, title, csv, summary) {
    ## Make graph for DATA and save it in 'NAME.png'.  TYPE is type of
    ## data representation on graph, like "o", "l" or "p". TITLE is
    ## the title of graph. CSV and SUMMARY is boolean for saving
    ## or not DATA on files.
    v_b     <- data_to_number (data[,"b"])
    v_c     <- data_to_number (data[,"c"])
    v_p     <- data_to_number (data[,"pack"])
    v_new   <- data_to_number (data[,"new"])
    v_delta <- data_to_number (data[,"delta"])

    ## FIXME
    ## gnuplot_3d_lines(data, type = dtype, name = name, mean = FALSE)

    ## None representative data
    if (length (v_p) < 5)
        return ()

    if (csv) {
        write.csv (data, paste ("csv/", name, ".csv", sep = ""),
                   row.names = FALSE)
    }

    make_histogram (data, name)

    ## Plot
    png (file = paste ("png/", name, ".png", sep = ""),
         width=graph_size, height=graph_size)

    plot (x = v_new,
          xlim = c(0, length(v_p)),
          ylim = c(min(v_c, v_new, v_b),
                   max(v_c, v_new, v_b)),
          type = type, col = "red",
          xlab = "Data", ylab = "Size (KBytes)",
          main = title)

    lines (v_b,    type = type, col = "purple")
    lines (v_new,  type = type, col = "red")
    lines (v_c,    type = type, col = "blue")

    legend ('topleft', lty=1, bty = 'n', cex=.75,
            legend = c("New (c)", "Old (c)", "Input matrix"),
            col    = c("red", "blue", "purple"))

    dev.off ()
}

axpx_make_graph <- function (data, name, dtype, type, title, csv, summary) {
    ## Make graph for DATA and save it in 'NAME.png'.  TYPE is type of
    ## data representation on graph, like "o", "l" or "p". TITLE is
    ## the title of graph. CSV and SUMMARY is boolean for saving
    ## or not DATA on files.
    v_b     <- data_to_number (data[,"b"])
    v_c     <- data_to_number (data[,"c"])
    v_p     <- data_to_number (data[,"pack"])
    v_new   <- data_to_number (data[,"new"])
    v_delta <- data_to_number (data[,"delta"])

    ## FIXME
    ## gnuplot_3d_lines(data, type = dtype, name = name, mean = FALSE)

    ## None representative data
    if (length (v_p) < 5)
        return ()

    if (csv) {
        write.csv (data, paste ("csv/", name, ".csv", sep = ""),
                   row.names = FALSE)
    }

    make_histogram (data, name)

    ## Plot
    png (file = paste ("png/", name, ".png", sep = ""),
         width=graph_size, height=graph_size)

    plot (x = v_new,
          xlim = c(0, length(v_p)),
          ylim = c(min(v_c, v_new, v_b),
                   max(v_c, v_new, v_b)),
          type = type, col = "red",
          xlab = "Data", ylab = "Size (KBytes)",
          main = title)

    lines (v_b,    type = type, col = "purple")
    lines (v_new,  type = type, col = "red")
    lines (v_c,    type = type, col = "blue")

    legend ('topleft', lty=1, bty = 'n', cex=.75,
            legend = c("New (c)", "Old (c)", "Input matrix"),
            col    = c("red", "blue", "purple"))

    dev.off ()
}
